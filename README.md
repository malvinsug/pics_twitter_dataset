# IMPORTANT REMARKS

- The dataset in this repository is firstly processed by Tiantian He. 
  The dataset contained 2511 vertices, 37154 edges, and 9073 node 
  attributes representing twitter users, friendship between these users, 
  and user profile and words in tweets, respectively. 
    
  I proccesed the data furthermore by removing mentions data. So what
  we have as attributes are only hashtags. (2511 vertices and 1966 attributes)

- The PICS algorithm is not provided in this repository. You can
  can get the algorithm from this link. 

  http://www.andrew.cmu.edu/user/lakoglu/pubs.html#pics

- In order to create the result diagram of matrix A and matrix F, 
  you should write this on command window:

      load('Twitter_OnlyHashtags_Result.mat')

      figure;
      plot_binary_duo(F2,1,3);
      plot_cluster_grid(nx, ny, Nx, Ny);

      figure;
      plot_binary_duo(A2,1,1);
      plot_cluster_grid(nx, nx, Nx, Nx);


# CITATION

- L.Akoglu, H.Tong, B.Meeder, C.Faloutsos PICS: parameter-free identification of 
cohesive sub-groups in large attributed graphs. Proceedings of the 12th SIAM 
international conference on data mining(SDM), Anaheim, CA. SIAM/Omnipress, pp 439–450, 2012.

- T. He, and K.C.C. Chan, "Discovering Fuzzy Structural Patterns for Graph Analytics," 
IEEE Transactions on Fuzzy Systems, vol. 26, no. 5, pp. 2785-2796, 2018.

- T. He, and K.C.C. Chan, "MISAGA: An Algorithm for Mining Interesting Subgraphs in 
Attributed Graphs," IEEE Transactions on Cybernetics, vol. 48, no. 5, pp. 1369-1382, 2018.